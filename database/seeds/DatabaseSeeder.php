<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credits = factory(\App\Models\Credit::class, 50)->create();


        factory(\App\Models\Asset::class, 25)->create()->each(function($asset) use ($credits) {
            $num = array_rand([1,2,3,4,5]);
            $used = [];
            for ($i=0; $i<$num; $i++) {
                $credit = $credits->random();
                if (in_array($credit->id, $used)) continue;
                $asset->credits()->save($credit);
                $used[] = $credit->id;
            }
        });
    }
}
