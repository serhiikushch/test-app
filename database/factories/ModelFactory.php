<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//$factory->define(App\User::class, function ($faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->email,
//    ];
//});


$factory->define(App\Models\Asset::class, function($faker) {
    return [
        'title' => $faker->text(10),
        'description' => $faker->text,
        'thumbnail' => $faker->image,
    ];
});



$factory->define(App\Models\Credit::class, function($faker) {
    return [
        'title' => $faker->firstName . ' ' . $faker->lastName,
        'role' => function(){
            $roles = ['client', 'agency', 'prouction co', 'award winner', 'director', 'editorial'];
            $key = array_rand($roles);
            return $roles[$key];
        },
    ];
});


