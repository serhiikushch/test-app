<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

/*
 * Assets endpoint
 */

$app->get('assets', ['uses' => 'AssetsController@index']);
$app->get('assets/{id}', ['as' => 'assets.show', 'uses' => 'AssetsController@show']);
$app->get('assets/{id}/credits', ['as' => 'assets.credits', 'uses' => 'AssetsController@showAssetCredits']);
$app->post('assets', ['uses' => 'AssetsController@create']);
$app->patch('assets/{id}', ['uses' => 'AssetsController@update']);
$app->delete('assets/{id}', ['uses' => 'AssetsController@delete']);

/*
 * Credits endpoint
 */

$app->get('credits', ['uses' => 'CreditsController@index']);
$app->get('credits/{id}', ['as' => 'credits.show', 'uses' => 'CreditsController@show']);
$app->get('credits/{id}/assets', ['as' => 'credits.assets', 'uses' => 'CreditsController@showCreditsAssets']);
$app->post('credits', ['uses' => 'CreditsController@create']);
$app->patch('credits/{id}', ['uses' => 'CreditsController@update']);
$app->delete('credits/{id}', ['uses' => 'CreditsController@delete']);