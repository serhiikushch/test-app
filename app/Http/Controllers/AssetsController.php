<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 29.03.16
 * Time: 9:46
 */

namespace App\Http\Controllers;


use App\Models\Asset;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AssetsController extends Controller
{
    public function index(Request $request)
    {
        $order = $request->input('sort', 'asc');
        $order = in_array($order, ['asc', 'desc']) ? $order : 'asc';
        $assets = Asset::orderBy('title', $order);
        if (($limit = (int)$request->input('limit')) && $limit > 0) {
            $assets->take($limit);
        }

        return response()->json($assets->get());
    }

    public function show($id)
    {
        try {
            $response = response()->json(Asset::findOrFail($id));
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 404);
        }

        return $response;
    }

    public function showAssetCredits($id)
    {
        try {
            $response = response()->json(Asset::findOrFail($id)->credits()->get());
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 404);
        }

        return $response;
    }

    public function create(Request $request)
    {
        try {
            $this->validate($request, array(
                'asset.title' => 'required',
                'asset.description' => 'required',
                'asset.thumbnail' => 'required'
            ));
            $asset = new Asset($request->input('asset'));
            if ($asset->save()) {
                $response = response('', 201, ['Location' => route('assets.show', ['id' => $asset->id])]);
            } else {
                throw new \Exception('Server error');
            }
        } catch (ValidationException $e) {
            $response = $e->response;
            $response->setData(['error' => $response->getData()]);
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 500);
        }

        return $response;
    }

    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, array(
                'asset.title' => 'sometimes|required',
                'asset.description' => 'sometimes|required',
                'asset.thumbnail' => 'sometimes|required'
            ));
            $asset = Asset::findOrFail($id);
            $asset_data = $request->input('asset');
            $asset->fill($asset_data);
            if ($asset->save()) {
                if (isset($asset_data['credits'])) {
                    if ($asset_data['credits']) {
                        if (!is_array($asset_data['credits'])) $asset_data['credits'] = array($asset_data['credits']);
                        $asset->credits()->sync($asset_data['credits']);
                    } else {
                        $asset->credits()->detach();
                    }
                }
                $response =  response('', 204);
            }
        } catch (ValidationException $e) {
            $response = $e->response;
            $response->setData(['error' => $response->getData()]);
        } catch (ModelNotFoundException $e) {
            $response = response(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 500);
        }

        return $response;

    }

    public function delete($id)
    {
        $asset = Asset::find($id);
        if ($asset) {
            if ($asset->delete()) {
                return response('OK', 200);
            }
        } else {
            return response('Not found', 404);
        }
    }



}