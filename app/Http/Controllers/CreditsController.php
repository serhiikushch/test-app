<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 29.03.16
 * Time: 22:05
 */

namespace App\Http\Controllers;

use app\Models\Asset;
use App\Models\Credit;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CreditsController extends Controller
{
    public function index(Request $request)
    {
        if (($limit = (int)$request->input('limit')) && $limit > 0) {
            $response = Credit::take($limit)->get();
        } else {
            $response = Credit::all();
        }

        return response()->json($response);
    }

    public function show($id)
    {
        try {
            $response = response()->json(Credit::findOrFail((int)$id));
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 404);
        }
        return $response;
    }
    
    public function showCreditsAssets($id)
    {
        try {
            $response = response()->json(Credit::findOrFail($id)->assets()->get());
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 404);
        }

        return $response;
    }

    public function create(Request $request)
    {
        try {
            $this->validateIncomingAssetData($request);
            $credit = new Credit($request->input('credit'));
            if ($credit->save()) {
                $response = response('OK', 201, ['Location' => route('credits.show', ['id' => $credit->id])]);
            } else {
                throw new \Exception('Server error');
            }
        } catch (ValidationException $e) {
            $response = $e->response;
            $response->setData(['error' => $response->getData()]);
        } catch (\Exception $e) {
            $response = response(['error' => $e->getMessage()], 500);
        }

        return $response;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'credit.title' => 'sometimes|required',
            'credit.role' => 'sometimes|required'
        ));
        $credit = Credit::find($id);
        if ($credit) {
            $credit_data = $request->input('credit');
            $credit->fill($credit_data);
            if ($credit->save()) {
                if (isset($credit_data['assets'])) {
                    if ($credit_data['assets']) {
                        if (!is_array($credit_data['assets'])) $credit_data['assets'] = array($credit_data['assets']);
                        $credit->assets()->sync($credit_data['assets']);
                    } else {
                        $credit->assets()->detach();
                    }
                }
                return response('', 204);
            }
        } else {
            return response('Not found', 404);
        }
    }

    public function delete($id)
    {
        $credit = Credit::find($id);
        if ($credit) {
            if ($credit->delete()) {
                return response('OK', 200);
            }
        } else {
            return response('Not found', 404);
        }
    }

    private function validateIncomingAssetData(Request $request)
    {
        $this->validate($request, array(
            'credit.title' => 'required',
            'credit.role' => 'required'
        ));
    }
}