<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 29.03.16
 * Time: 21:57
 */

namespace app\Models;


use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'role'];
    protected $hidden = ['pivot'];

    public function assets() {
        return $this->belongsToMany('App\Models\Asset');
    }
}