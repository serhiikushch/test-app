<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 29.03.16
 * Time: 9:51
 */

namespace app\Models;


use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = ['title', 'description', 'thumbnail'];
    protected $hidden = ['pivot'];

    public function credits() {
        return $this->belongsToMany('App\Models\Credit');
    }
}