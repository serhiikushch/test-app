# Installation instructions


## System equirements
 - PHP >= 5.5.9
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - SQLite
 - MYSQL
 - Composer



## Installation steps
 - clone repository `git clone git@bitbucket.org:kusch/test-app.git` or unpack application archive
 - enter app directory `cd test-app`
 - install dependencies `composer install`
 - create environment file `cp .env.example .env`
 - create database 'wiredrive' (off-course name without quotes)
 - set your mysql user `DB_USERNAME` and password `DB_PASSWORD` in environment file '.env'
 - apply migrations to the database `php artisan migrate`
 - create some data for application `php artisan db:seed`
 - run built-in php-server `php -S 0.0.0.0:8000 -t public/`
 - now application is available via http://127.0.0.1:8000


## About application

It is build on microfamework Lumen https://lumen.laravel.com/
All endpoints are in app/Http/Controllers, models - in app/Models
Application routes are defined in app/Http/routes.php

### Api urls
 - /assets - get all available assets
 - /assets?limit=x - to request 'x' rows
 - /assets?sort=desc - to sort in reverse alphabetical order. by default items ordered by title asc
 - /assets/{asset_id} - to get asset
 - POST /assets/{asset_id} - to create an asset.
    Example request: `curl -v --data "asset[title]=my asset&asset[description]=my description&asset[thumbnail]=http://placehold.it/200x200" http://127.0.0.1:8000/assets`
 - PATCH /assets/{asset_id}  - to edit asset
    To associate with credits: `curl -v --request PATCH --data "asset[credits][]={credit_id}&asset[credits][]={one_more_credit_id}" http://127.0.0.1:8000/assets/{asset_id}`
 - /assets/{asset_id}/credits - to get asset credits
 - DELETE /assets/{asset_id} - to delete asset

The same actions are available for credits. Credits urls start with /credits
To associate credit with asset use PATCH request:
    `curl -v --request PATCH --data "credit[assets][]={asset_id}&credit[assets][]={one_more_asset_id}" http://127.0.0.1:8000/credits/{credit_id}`

To get all assets by credits use /credits/{credit_id}/assets


## Tests

Test environment uses sqlite database. Please, ensure you have installed all the requirements.
To run the tests execute `./vendor/phpunit/phpunit/phpunit`

