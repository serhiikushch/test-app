<?php
/**
 * Created by PhpStorm.
 * User: sk
 * Date: 29.03.16
 * Time: 22:53
 */

namespace tests;


class CreditsControllerTest extends \TestCase
{

    use \Laravel\Lumen\Testing\DatabaseMigrations;

    public function testListCredits()
    {
        $credits = factory(\App\Models\Credit::class, 3)->create();
        $response = $this->get('/credits');
        $response->assertResponseOk();
        $response->seeJsonEquals($credits->toArray());
    }

    public function testShowCredit()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $response = $this->get("/credits/{$credit->id}");
        $response->assertResponseOk();
        $response->seeJsonEquals($credit->toArray());
    }

    public function testCreditIsNotFound()
    {
        $response = $this->get('/credits/111111');
        $response->assertResponseStatus(404);
    }


    public function testCreateCredit()
    {
        $credit = factory(\App\Models\Credit::class)->make();

        $response = $this->post('/credits', array(
            'credit' => [
                'title' => $credit->title,
                'role' => $credit->role
            ]
        ));

        $this->seeInDatabase('credits', ['title' => $credit->title,
            'role' => $credit->role]);
        $response->assertResponseStatus(201);
    }

    public function testCreditAssetWithInvalidData()
    {
        $response = $this->post('/credits');
        $response->assertResponseStatus(422);
    }

    public function testUpdateCredit()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $newTitle = 'just changed title';
        $response = $this->patch("/credits/{$credit->id}", ['credit' => ['title' => $newTitle]]);
        $response->assertResponseStatus(204);
        $this->seeInDatabase('credits', ['title' => $newTitle,
            'role' => $credit->role]);
    }

    public function testUpdateAssetWithInvalidData()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $newTitle = '';
        $response = $this->patch("/credits/{$credit->id}", ['credit' => ['title' => $newTitle]]);

        $response->assertResponseStatus(422);
    }

    public function testUpdateUnexistedAsset() {
        $response = $this->patch("/credits/11111", ['credit' => ['title' => 'hello world!']]);
        $response->assertResponseStatus(404);
    }

    public function testDeleteAsset()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $this->seeInDatabase('credits', $credit->toArray());

        $response = $this->delete("/credits/{$credit->id}");
        $response->assertResponseStatus(200);
        $this->notSeeInDatabase('credits', $credit->toArray());
    }

    public function testDeleteUnexistedAsset()
    {

        $response = $this->delete("/credits/11111");
        $response->assertResponseStatus(404);
    }

    public function testRetrieveCreditAssets()
    {
        $assets = factory(\App\Models\Asset::class, 4)->make();
        $credit = factory(\App\Models\Credit::class)->create();
        foreach ($assets as $asset) {
            $credit->assets()->save($asset);
        }

        $response = $this->get("/credits/{$credit->id}/assets");
        $response->assertResponseStatus(200);
//
        $response->seeJsonEquals($assets->toArray());
    }

    public function testCanAssociateWithAsset()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $assets = factory(\App\Models\Asset::class, 4)->create();
        $this->patch("/credits/{$credit->id}", [
            'credit' => ['assets' => $assets->pluck('id')->all()]]);

        $response = $this->get("/credits/{$credit->id}/assets");
        $response->seeJsonEquals($assets->toArray());
    }

    public function testCanDropAssetAssociations()
    {
        $credit = factory(\App\Models\Credit::class)->create();
        $assets = factory(\App\Models\Asset::class, 4)->create();
        $credit->assets()->attach($assets->pluck('id')->all());
        $response = $this->get("/credits/{$credit->id}/assets");
        $response->seeJsonEquals($assets->toArray());

        $response = $this->patch("/credits/{$credit->id}", [
            'credit' => ['assets' => []]]);

        $response = $this->get("/credits/{$credit->id}/assets");
        $response->seeJsonEquals([]);
    }

}