<?php


namespace tests;


class AssetsControllerTest extends \TestCase
{

    use \Laravel\Lumen\Testing\DatabaseMigrations;



    public function testListAssets()
    {
        $assets = factory(\App\Models\Asset::class, 3)->create();
        $response = $this->get('/assets');
        $response->assertResponseOk();
        $response->seeJsonEquals($assets->toArray());
    }

    public function testShowAsset()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $response = $this->get("/assets/{$asset->id}");
        $response->assertResponseOk();
        $response->seeJsonEquals($asset->toArray());
    }

    public function testAssetIsNotFound()
    {
        $response = $this->get('/assets/111111');
        $response->assertResponseStatus(404);
    }


    public function testCreateAsset()
    {
        $asset = factory(\App\Models\Asset::class)->make();

        $response = $this->post('/assets', array(
            'asset' => [
                'title' => $asset->title,
                'description' => $asset->description,
                'thumbnail' => $asset->thumbnail,
            ]
        ));

        $this->seeInDatabase('assets', ['title' => $asset->title,
            'description' => $asset->description,
            'thumbnail' => $asset->thumbnail]);
        $response->assertResponseStatus(201);
    }

    public function testCreateAssetWithInvalidData()
    {
        $response = $this->post('/assets');
        $response->assertResponseStatus(422);
    }

    public function testUpdateAsset()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $newTitle = 'just changed title';
        $response = $this->patch("/assets/{$asset->id}", ['asset' => ['title' => $newTitle]]);
        $this->seeInDatabase('assets', ['title' => $newTitle,
            'description' => $asset->description,
            'thumbnail' => $asset->thumbnail]);
        $response->assertResponseStatus(204);
    }

    public function testUpdateAssetWithInvalidData()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $newTitle = '';
        $response = $this->patch("/assets/{$asset->id}", ['asset' => ['title' => $newTitle]]);

        $response->assertResponseStatus(422);
    }

    public function testUpdateUnexistedAsset() {
        $response = $this->patch("/assets/11111", ['asset' => ['title' => 'hello world!']]);
        $response->assertResponseStatus(404);
    }

    public function testDeleteAsset()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $this->seeInDatabase('assets', $asset->toArray());

        $response = $this->delete("/assets/{$asset->id}");
        $response->assertResponseStatus(200);
        $this->notSeeInDatabase('assets', $asset->toArray());
    }

    public function testDeleteUnexistedAsset()
    {

        $response = $this->delete("/assets/11111");
        $response->assertResponseStatus(404);
    }

    public function testRetrieveAssetCredits()
    {
        $credits = factory(\App\Models\Credit::class, 4)->make();
        $asset = factory(\App\Models\Asset::class)->create();
        foreach ($credits as $credit) {
            $asset->credits()->save($credit);
        }

        $response = $this->get("/assets/{$asset->id}/credits");
        $response->assertResponseStatus(200);
//
        $response->seeJsonEquals($credits->toArray());
    }

    public function testCanAssociateWithCredits()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $credits = factory(\App\Models\Credit::class, 4)->create();
        $this->patch("/assets/{$asset->id}", [
            'asset' => ['credits' => $credits->pluck('id')->all()]]);

        $response = $this->get("/assets/{$asset->id}/credits");
        $response->seeJsonEquals($credits->toArray());
    }

    public function testCanDropCreditAssociations()
    {
        $asset = factory(\App\Models\Asset::class)->create();
        $credits = factory(\App\Models\Credit::class, 4)->create();
        $asset->credits()->attach($credits->pluck('id')->all());
        $response = $this->get("/assets/{$asset->id}/credits");
        $response->seeJsonEquals($credits->toArray());

        $response = $this->patch("/assets/{$asset->id}", [
            'asset' => ['credits' => []]]);

        $response = $this->get("/assets/{$asset->id}/credits");
        $response->seeJsonEquals([]);
    }

}